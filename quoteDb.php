<?php

class QuoteDb
{

    protected $mysql;
    const DB_HOST = '127.0.0.1';
    const DB_PORT = '3306';
    const DB_USER = 'root';
    const DB_PASS = 'wordpress';
    const DB_NAME = 'FavQuotes';


    /**
     * Constructor
     */
    public function __construct()
    {
        try {
            $this->mysql = new mysqli(self::DB_HOST, self::DB_USER, self::DB_PASS, self::DB_NAME, self::DB_PORT);
        } catch (mysqli_sql_exception $e) {
            http_response_code(500);
            exit;
        }
    }


    /**
     * Devuelve todas las entradas
     * @return mixed
     */
    public function getAllQuotes()
    {
        $query = $this->mysql->prepare("SELECT * FROM quotes ; ");
        $query->execute();
        $result = $query->get_result();
        $quotes = $result->fetch_all(MYSQLI_ASSOC);
        $query->close();
        return $quotes;
    }

    /**
     * @param $user_id
     * @return bool
     * @desc Lista de entradas tipo Quote marcadas como favoritas.
     **/
    public function getMyFavs($user_id)
    {
        $query = $this->mysql->prepare("SELECT quotes.quote FROM quotes RIGHT JOIN Favs ON quotes.id = Favs.quote_id AND Favs.user_id=?;");
        $query->bind_param($user_id);

        $query->execute();
        $result = $query->get_result();
        $query->close();

        if ($result) {
            return true;
        }
        return false;

    }

    /**
     * @param $user_id
     * @return bool
     * @desc Devuelve una entrada tipo Quote
     **/
    public function getQuote($id = 0)
    {
        $stmt = $this->mysqli->prepare("SELECT * FROM quotes WHERE id=? ; ");
        $stmt->bind_param($id);
        $stmt->execute();
        $result = $stmt->get_result();
        $peoples = $result->fetch_all(MYSQLI_ASSOC);

        $stmt->close();
        return $peoples;
    }

    /**
     * @param $user_id
     * @param $quote_id
     * @return bool
     * @Desc Marca como favorita una Quote
     */
    public function setFav($user_id, $quote_id)
    {

        $query = $this->mysql->prepare("INSERT INTO `FavQuotes`.`Favs` (`user_id`, `quote_id`) VALUES (?, ?);");
        $query->bind_param($user_id, $quote_id);
        $query->execute();
        $result = $query->get_result();
        $quotes = $result->fetch_all(MYSQLI_ASSOC);
        $query->close();
        if ($result) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return bool
     * @dec Genera una entrada de elemento tipo Quote
     */
    public function setQuote($data)
    {

        $query = $this->mysql->prepare("INSERT INTO `FavQuotes`.`quotes` (`quote`, `author_id`) VALUES ('?', ?);");
        $query->bind_param($data["quote"], $data["uid"]);
        $query->execute();
        $result = $query->get_result();

        $query->close();
        if ($result) {
            return true;
        }
        return false;
    }

}

