<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/QuoteController.php';

$klein = new \Klein\Klein();
$qc = new QuoteController();


// Rutas de las Quotes
$klein->respond('GET', '/quotes', $qc->getQuotes());
$klein->respond('POST', '/quotes', $qc->setQuote($request->body));
$klein->respond('GET', '/quotes/[i:id]', $qc->getQuotes($request->param($id, $default = 0)));

// En referencia a favoritos
$klein->respond('GET', '/favs', $qc->myFavs());
$klein->respond('POST', '/favs', $qc->setFav());

$klein->dispatch();

