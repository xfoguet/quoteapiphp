<?php
require_once "quoteDb.php";

class QuoteController
{
    /**
     *
     */
    public function getQuotes()
    {
        $db = new QuoteDb();
        $response = $db->getAllQuotes();

        echo json_encode($response, JSON_PRETTY_PRINT);
        exit;
    }

    /**
     * @param $id
     */
    public function getOneQuote($id)
    {
        $db = new QuoteDb();
        $response = $db->getQuote($id);
        echo json_encode($response, JSON_PRETTY_PRINT);
        exit;
    }

    /**
     * @param $data
     */
    public function setQuote($data)
    {
        $uid = $data->uidset;
        $db = new  QuoteDb();
        $response = $db->setQuote($data, $uid);

        echo json_encode($response, JSON_PRETTY_PRINT);
        exit;
    }

    /**
     * @param $data
     */
    public function setFav($data)
    {
        $quote_id = $data["id"];
        $user_id = $data["uid"]; //en el mundo real se miraría el id de la cookie se sessión,  JWT.. who kown

        $db = new  QuoteDb();
        $response = $db->setFav($user_id, $quote_id);

        // Lo suyo sería tener una clase de errores para mandar el json con la cabecera que tocase.
        // Lo imaginamos muy imaginado, mientras..
        if (!$response) {
            echo json_encode($response, JSON_PRETTY_PRINT);
        }

        echo json_encode($response, JSON_PRETTY_PRINT);
        exit;
    }


    /**
     * @param $data
     */
    public function myFavs($data)
    {
        $user_id = $data["uid"];
        $db = new QuoteDb();
        $response = $db->getMyFavs($user_id);

        echo json_encode($response, JSON_PRETTY_PRINT);
        exit;
    }


}

